CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers



INTRODUCTION
------------
Full introduction can be found here:
https://joshideas.com/ji_quickbooks.

REQUIREMENTS
------------
Necessary requirements can be found at https://joshideas.com/ji_quickbooks.


INSTALLATION
------------
Installation information can be found here:
https://joshideas.com/ji_quickbooks.

CONFIGURATION
-------------
Please visit https://joshideas.com/ji_quickbooks for more information.

TROUBLESHOOTING
---------------
Post any issue you may have in our issue queue
https://www.drupal.org/project/issues/2847007?status=All&categories=All page.

You can see our actively maintained documentation here
https://joshideas.com/ji_quickbooks

FAQ
--------------
Q: Why isn't this readme.txt full of information?
A: To much work to maintain two readme/documentation areas. Please visit
   https://joshideas.com/ji_quickbooks for up to date documentation.

MAINTAINERS
-----------
Current maintainers:
 * Joshua Ramirez  https://www.drupal.org/u/joshideas - https://joshideas.com

<?php

/**
 * @file
 */
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Item;

/**
 * Drupal get form callback.
 */
function ji_products_settings($form, &$form_state) {
  if (ji_quickbooks_check_common_errors()) {
    return;
  }

  if (!ji_quickbooks_load_library()) {
    return;
  }

  $form['enable_saving_entity'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync when a product is updated.'),
    '#default_value' => variable_get('ji_quickbooks_enable_saving_entity', 0),
    '#description' => t('This can happen if someone updates the inventory within the admin panel or someone makes a purchase which fires the decrease inventory Rule.'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['qbo_to_drupal'] = array(
    '#type' => 'fieldset',
    '#title' => t('QBO to Drupal Sync Options'),
    '#description' => t('Queries QBO 100 records at a time until no more records are found. Compares QBO product SKUs to Commerce Product SKUs. Updates inventory within Drupal.'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['qbo_to_drupal']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Sync Product Inventory to Drupal'),
    '#submit' => array('ji_products_sync_inventory_to_drupal'),
  );

  $form['drupal_to_qbo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Drupal to QBO Sync Options'),
    '#description' => t('Queries QBO 100 records at a time, comparing those SKUs to Commerce products if it finds a match, sends inventory values from Drupal to QBO.'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#disabled' => TRUE,
  );

  $form['drupal_to_qbo']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Sync Product Inventory to QBO'),
    '#submit' => array('ji_products_sync_inventory_to_qbo'),
  );

  return $form;
}

/**
 * Submit callback.
 */
function ji_products_settings_submit($form, &$form_state) {
  if (isset($form_state['values']['enable_saving_entity'])) {
    // Allows hook_entity_presave() to work.
    variable_set('ji_quickbooks_enable_saving_entity', $form_state['values']['enable_saving_entity']);
  }
}

/**
 * Sync QBO products to Drupal Commerce products.
 */
function ji_products_sync_inventory_to_drupal($form, &$form_state) {
  $batch = array(
    'title' => t('Bulk Pull QuickBooks Product Inventory'),
    'operations' => array(
      array('ji_products_get_total_items', array()),
      array('ji_products_get_inventory', array()),
    ),
    'finished' => 'ji_products_get_inventory_finished',
  );

  $batch['file'] = drupal_get_path('module', 'ji_products') . '/ji_products.admin.inc';

  batch_set($batch);
}

/**
 * Batch callback; Get total Items from QBO.
 */
function ji_products_get_total_items(&$context) {
  $quickbooks_service = new JIQuickBooksService();

  $context['sandbox']['total'] = $quickbooks_service->getCountOf('item');
  $context['message'] = t('Retrieved total record count: @count', array('@count' => $context['sandbox']['total']));
}

/**
 * Batch callback; Get Inventory Items from QBO.
 */
function ji_products_get_inventory(&$context) {
  $quickbooks_service = new JIQuickBooksService();

  // Check if we're starting fresh.
  if (!isset($context['sandbox']['count'])) {
    $context['sandbox']['count'] = 1;
    // Limit so we don't exhaust server resources.
    $context['sandbox']['max'] = 100;
  }

  $filter = array(
    'limit' => array(
      'start' => $context['sandbox']['count'],
      'end' => $context['sandbox']['max'])
  );

  $response = $quickbooks_service->getAllItems('Inventory', $filter);

  // We must have finished. Returns NULL if we can't find any more items.
  if (is_null($response)) {
    $context['finished'] = 1;
    return;
  }

  foreach ($response as $product) {
    /* @var $product QuickBooksOnline\API\Data\IPPItem */
    $product_object = commerce_product_load_by_sku($product->Sku);
    if ($product_object) {
      $product_object->commerce_stock['und'][0]['value'] = $product->QtyOnHand;
      $product_object->field_product_cost['und'][0]['amount'] = $product->PurchaseCost * 100;
      // @TODO: We shouldn't be forcing the currency_code. We do this
      // because there's a database access violation if currency_code is
      // missing, i.e. - From products that haven't been updated since
      // this is a new field added to all existing products.
      $product_object->field_product_cost['und'][0]['currency_code'] = 'USD';
      commerce_product_save($product_object);
    }
  }

  $context['sandbox']['count'] += count($response);
  $context['message'] = t('Synced Inventory items: ' . ($context['sandbox']['count'] - 1));

  if ($context['sandbox']['total'] != $context['sandbox']['count']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

/**
 * Batch finished callback.
 */
function ji_products_get_inventory_finished($success, $results, $operations) {
  // The void.
}

/**
 * Sync Drupal Commerce products to QBO.
 */
function ji_products_sync_inventory_to_qbo($form, &$form_state) {
  $batch = array(
    'title' => t('Bulk Push QuickBooks Product Inventory'),
    'operations' => array(
      array('ji_products_push_inventory', array()),
    ),
    'finished' => 'ji_products_push_inventory_finished',
  );

  $batch['file'] = drupal_get_path('module', 'ji_products') . '/ji_products.admin.inc';

  batch_set($batch);
}

/**
 * Batch callback; Get Products and sync to QBO.
 */
function ji_products_push_inventory(&$context) {
  // Sleep to prevent hitting QBO's limits of 500 requests per minute.
  if (isset($context['sandbox']['count'])) {
    sleep(15);
  }

  // Check if we're starting fresh.
  if (!isset($context['sandbox']['count'])) {
    $context['sandbox']['count'] = 1;
    // Limit so we don't exhaust server resources.
    $context['sandbox']['max'] = 100;
  }

  $quickbooks_service = new JIQuickBooksService();

  $filter = array(
    'limit' => array(
      'start' => $context['sandbox']['count'],
      'end' => $context['sandbox']['max'])
  );

  $response = $quickbooks_service->getAllItems('Inventory', $filter);

  // We must have finished. Returns NULL if we can't find any more items.
  if (is_null($response)) {
    $context['finished'] = 1;
    return;
  }

  foreach ($response as $product) {
    /* @var $product QuickBooksOnline\API\Data\IPPItem */
    $product_object = commerce_product_load_by_sku($product->Sku);
    if ($product_object) {
      $theResourceObj = Item::update($product, [
          "QtyOnHand" => $product_object->commerce_stock['und'][0]['value'],
      ]);

      $resultingCustomerUpdatedObj = $quickbooks_service->dataService->Update($theResourceObj);
      if ($error) {
        echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
        echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
        echo "The Response message is: " . $error->getResponseBody() . "\n";
        break;
      }

      $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingCustomerUpdatedObj, $urlResource);
    }
  }

  $context['sandbox']['count'] += count($response);
  $context['message'] = t('Synced products to QBO: ' . $context['sandbox']['count']);

  if ($context['sandbox']['total'] != $context['sandbox']['count']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

/**
 * Batch finished callback.
 */
function ji_products_push_inventory_finished($success, $results, $operations) {
  // The void.
}
